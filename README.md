# Discord Dungeons and Dragons Bot
Python 3.5 and the necessary python packages are required to use the bot.

Necessary packages include:
    discord,
    pandas,
    tabulate
    
You can install the packages with pip.
