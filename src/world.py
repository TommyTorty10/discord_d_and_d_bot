from noise import snoise2
from numpy import array as np
from scipy.interpolate import interp1d
import pandas as pd
import random
from character import Character


class World:
	def __init__(self):
		self.players = {}
		self.enemies = {}

		self.world, self.noiselist, self.width = self.pnoise()
		for index, row in self.world.iterrows():
			for col in self.world:
				place = self.world.loc[index, col]
				m = interp1d([-1, 1], [0, 100])
				temp = {'temp': float(m(place['noise']))}  # refer above within this function for m
				var = self.world.loc[index, col]
				var = {**var, **temp}
				self.world.at[index, col] = var  # append temperature to dictionaries in dataframe

	@staticmethod
	def pnoise(seed=0.0, width=25):
		"""Returns
		a pandas DataFrame of pnoise,
		a list of the same pnoise,
		the width of the DataFrame (which happens to be the same as the length)."""
		letters = ['x', 's', 't', 'T', 'S', 'v', 'V', 'h', 'w', 'I', 'N', 'E', 'G', 'L', 'A', 'J', 'k', 'y', 'R',
				   'K', 'g',
				   'j', 'z', 'D', 'c']
		pnoise = []
		octaves = 27
		# freq = 16.0 * octaves
		for y in range(width):
			for x in range(width):
				pnoise.append(snoise2(x, y, octaves, base=seed))
		# changes datatype in cells to 'object' from float64.  Use print(df.dtypes) to show data types
		df = pd.DataFrame(np(pnoise).reshape(width, width), columns=letters).astype('object')

		for index, row in df.iterrows():
			for col in df:
				d = {'noise': row[col], 'row': index, 'column': col}
				df.at[index, col] = d

		return df, pnoise, width

	def biome(self, location):
		"""Returns a biome string based on a given location (list with 2 ints)."""
		temp = self.world.iloc[location[0], location[1]]['temp']

		if temp >= 90:
			return 'desert'
		elif temp >= 80:
			return 'valley'
		else:
			return 'foothills'

	def create_player(self, member):
		player = Character(member)
		if member not in self.players.keys():
			self.players[member] = player
			return f'{member} has joined the game!'
		else:
			return 'You are already in the game.'

	def create_enemies(self, location):
		"""Returns a list of enemy characters."""
		num = random.randint(1, 5)
		for i in range(num):
			enemy = Character('enemy'+str(i))
			enemy.location = location
			self.enemies[enemy.name] = enemy
