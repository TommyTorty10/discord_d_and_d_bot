import os
import random


class Character:
	def __init__(self, name):
		if "game_data" not in os.getcwd():
			os.chdir("game_data")

		self.class_ = 'default'
		self.played = []
		self.location = [0, 0]  # on an x-y plane

		self.name = name
		self.health = random.randint(1, 50)
		self.attack = random.randint(1, 25)
		self.armor = random.randint(0, 23)

	def go_north(self):
		self.location[1] += 1

	def go_south(self):
		self.location[1] -= 1

	def go_east(self):
		self.location[0] += 1

	def go_west(self):
		self.location[0] -= 1
