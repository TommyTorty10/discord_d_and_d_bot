from character import Character
import unittest
from world import World


class Tests(unittest.TestCase):

	def test_biome_distribution(self):
		"""test that the world has more than one biome in a finite domain"""
		world = World()
		biomes = []
		for i in range(0, 25):
			for j in range(0, 25):
				biome = world.biome([i, j])
				if biome not in biomes:
					biomes.append(biome)

		self.assertTrue(1 < len(biomes))

	def test_create_player(self):
		"""test that players are created correctly without conflicts"""
		world = World()
		_ = world.create_player('Jane')
		_ = world.create_player('Jeb')
		conflicting_name = world.create_player('Jeb')
		# even thought the docs say .keys() returns a list, it actually returns a "dict:list", hence the conversion
		self.assertEqual(list(world.players.keys()), ['Jane', 'Jeb'])
		self.assertEqual(conflicting_name, 'You are already in the game.')

		player = world.players['Jane']
		self.assertTrue(isinstance(player, Character))
		self.assertEqual(player.name, 'Jane')

	def test_create_enemies(self):
		"""test that enemies are created correctly"""
		world = World()
		world.create_enemies([0,0])
		self.assertTrue(1 <= len(world.enemies) <= 5)

		i = 0  # just to know which enemy fails the test
		for enemy in world.enemies:  # the number of enemies is random, so for loop is necessary
			self.assertTrue(isinstance(world.enemies[enemy], Character))
			self.assertEqual(enemy, 'enemy'+str(i))
			i += 1

	def test_character_movement(self):
		"""test that Character's movement methods work properly"""
		world = World()
		_ = world.create_player('Jane')
		player = world.players['Jane']

		player.go_north()
		self.assertEqual(player.location, [0, 1])
		player.go_south()
		self.assertEqual(player.location, [0, 0])
		player.go_east()
		self.assertEqual(player.location, [1, 0])
		player.go_west()
		self.assertEqual(player.location, [0, 0])
