import random
import discord
from discord import Game
from discord.ext import commands
import os
from world import World

os.chdir("game_data")
BOT_PREFIX = ("?", "!")

f = open('bot_info.txt', 'r')
TOKEN = f.readline().rstrip()
f.close()

# global bot
intents = discord.Intents.default()
intents.message_content = True
bot = commands.Bot(command_prefix=BOT_PREFIX, intents=intents)
world = World()
played = []


# funtion titles are used as commands, so to run 8 ball func type !eight_ball
@bot.command(name='8ball',
             description="Answers a yes/no question.",
             brief="Answers from the beyond.",
             aliases=['eight_ball', 'eightball', '8-ball'],
             pass_context=True)
async def eight_ball(context):
    """Discord command.  Check discord command description with !help in discord."""
    possible_responses = [
        'That is a resounding no',
        'It is not looking likely',
        'Too hard to tell',
        'It is quite possible',
        'Definitely',
    ]
    await context.channel.send(random.choice(possible_responses) + ", " + context.message.author.mention)


@bot.event
async def on_ready():
    game = Game(name="with humans")
    await bot.change_presence(activity=game)
    print("Logged in as " + bot.user.name)


@bot.command(name='roll20',
             description="Rolls a d20 for you.",
             brief="Rolls a d20 for you.",
             aliases=['d20', 'D20'])
async def roll20(context):
    """Discord command.  Check discord command description with !help in discord."""
    chance = random.randint(1, 20)
    await context.channel.send(str(chance))


@bot.command(name='roll50',
             description="Rolls a d50 for you.",
             brief="Rolls a d50 for you.",
             aliases=['d50', 'D50'])
async def roll50(context):
    """Discord command.  Check discord command description with !help in discord."""
    chance = random.randint(1, 50)
    await context.channel.send(str(chance))


# @bot.command(name='create_world',
#              description="Creates your world.  It conjures a limitlessish landscape "
#                          "with all of the building blocks and mystery required for you to feel tragedy, "
#                          "romance, grief, heroism, and every other emotion.  In short you will feel life. ",
#              brief="Creates your world.",
#              aliases=['create_map', 'new_map', 'new_world'])
# async def create_world(context):
#     """Discord command.  Check discord command description with !help in discord."""
#     global world
#     world = World
#     await context.channel.send("New World Created!")


@bot.command(name='Print_Players',
             description="Lists all living players and their stats.",
             brief="Lists all living players and their stats.",
             aliases=['players', 'print_players'])
async def print_players(context):
    for player in world.players.keys():
        await context.channel.send(player)


@bot.command(name='print_enemies',
             description="Lists all present enemies and their stats",
             brief="Lists all enemies",
             aliases=['enemies', 'enemies_are'])
async def print_enemies(context):
    for enemy in world.enemies.keys():
        await context.channel.send(enemy + str(world.enemies[enemy].health))


@bot.command(name='Join_Game',
             description="Places you into the game of original_game.",
             brief="Places you into the game of original_game",
             aliases=['join', 'join_game', 'j'],
             pass_context=True)
async def join_game(context):
    """Discord command.  Check discord command description with !help in discord."""
    member = str(context.message.author)
    await context.channel.send(world.create_player(member))


@bot.command(name='New_Round',
             description="Starts a new round of the game.",
             brief="Starts a new round of the game.",
             aliases=['newround', 'new_round', 'nr'])
async def new_round(context):
    """Discord command.  Check discord command description with !help in discord."""
    global played
    played = []
    await context.channel.send('!!!New turn!!!\nYour enemies will attack now!')
    await enemy_attacks(context)
    if len(world.players) > 0:
        await context.channel.send('player healths are:')
        for player in world.players:
            await context.channel.send(world.players[player].name + ': '
                                       + str(world.players[player].health))
    else:
        await context.channel.send('All players are dead.')


def turn(player):
    """Checks if the given player has used their turn."""
    global played

    if world.players.keys():
        playernames = world.players.keys()
    else:
        playernames = []

    if player not in playernames:
        return 'not in game'
    if player not in played:
        played.append(player)
        outvar = 'turn used'
    else:
        outvar = 'turn already used'

    if len(played) == len(world.players):
        outvar += 'newturn'

    return outvar


@bot.command(name='Attack',
             description="Attacks an enemy based on a given integer.",
             brief="Attacks an enemy based on a given integer",
             aliases=['atk', 'attack'],
             pass_context=True)
async def attack(context, enemy):
    """Discord command.  Check discord command description with !help in discord."""
    enemy = 'enemy' + str(int(enemy))
    player = str(context.message.author)
    player_turn = turn(player)

    if 'turn already used' in player_turn:
        await context.channel.send('{} has already used her turn'.format(player))
        return
    if 'not in game' in player_turn:
        await context.channel.send(
            '{} has not joined the game.  Use the join_game command to do so.'.format(player))
        return

    if 'turn used' in player_turn:
        atk_dmg = world.players[player].attack
        world.enemies[enemy].health = world.enemies[enemy].health - atk_dmg

        if world.enemies[enemy].health <= 0:
            del world.enemies[enemy]
            await context.channel.send('{} has died.'.format(enemy))
            if len(world.enemies.keys()) > 0:
                await context.channel.send('enemies are \n')
                for e in world.enemies.keys():
                    await context.channel.send(e)
            else:
                await context.channel.send('All enemies are dead.')
        else:
            await context.channel.send('{} has now attacked {} which now has {} health'.format(
                player, enemy, world.enemies[enemy].health))
    if 'newturn' in player_turn:
        await context.channel.send(
            'Everyone has had a turn, and a new round should be started with the command new_round.')


@bot.event
async def enemy_attacks(context):
    """Plays the enemies' attacks against the players."""
    player = str(context.message.author)
    if not world.enemies:
        world.create_enemies(world.players[player].location)
    for enemy in world.enemies:
        if list(world.enemies[enemy].location) == list(world.players[player].location):
            player = random.choice(list(world.players.keys()))  # remeber .keys returns dict.keys, not actually a list
            new_health = world.players[player].health - world.enemies[enemy].attack
            if int(new_health) <= 0:
                del world.players[player]
                await context.channel.send(discord.Object(id='422216559704932353'),
                                           '{} has died'.format(str(player.index.values[0])))
            else:
                world.players[player].health = new_health


@bot.command(name='Set_Class',
             description="Sets the class of the player to a given string.",
             brief="Sets the class of the player to a given string",
             aliases=['setclass', 'set_class'],
             pass_context=True)
async def set_class(context, class_):
    """Discord command.  Check discord command description with !help in discord."""
    player = str(context.message.author)
    if player in world.players.keys():
        world.players[player].class_ = class_
        await context.channel.send('{} is now a '.format(player) + world.players['class'][player])
    else:
        await context.channel.send(
            '{} has not joined the game.  Use the join_game command to do so.'.format(player))


@bot.command(name='proceed',
             description="Puts the party in the next dungeon.  "
                         "Takes direction argument such as north, south, east, or west or n, s, e, w.",
             brief="Puts the party in the next dungeon.",
             aliases=['next_dungeon', 'p'],
             pass_context=True)
async def proceed(context):
    """Discord command.  Check discord command description with !help in discord."""
    if not world:
        await context.channel.send('You need to create a world with the create_world command.')
        return
    direction = str(context.message.content).lower()[-1]
    player = str(context.message.author)
    oldbiome = world.biome(world.players[player].location)

    if direction == 'north' or direction == 'n':
        world.players[player].go_north()
    elif direction == 'south' or direction == 's':
        world.players[player].go_south()
    elif direction == 'east' or direction == 'e':
        world.players[player].go_east()
    elif direction == 'west' or direction == 'w':
        world.players[player].go_west()
        print('w')
    else:
        await context.channel.send('Type a direction such as !proceed north  or  !proceed n.')
        return

    world.create_enemies(world.players[player].location)

    newbiome = world.biome(world.players[player].location)
    await context.channel.send('Your location is now ' + str(world.players[player].location))
    if newbiome != oldbiome:
        await context.channel.send('You are now in a ' + str(newbiome) + ' biome')

    await context.channel.send('Enemies present are \n')
    for enemy in world.enemies:
        await context.channel.send(str(enemy))


@bot.command(pass_context=True)
async def test(context, var):
    """Command to test basic connectivity of bot.  Check discord command description with !help in discord."""
    await context.channel.send(var + 'context: {}'.format(str(context.message.author)))


if __name__ == "__main__":
    bot.run(TOKEN)
